//
//  ListViewController.swift
//  Sample
//
//  Created by abc on 2018/03/20.
//  Copyright © 2018年 abc All rights reserved.
//

import UIKit
import UserNotifications

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell!
        
        cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // ハイライトを消す
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    @IBAction func addBtnDidPush(_ sender: UIBarButtonItem) {
        
        let notificationIdentifier = "notificationIdentifier"
        
        let content = UNMutableNotificationContent()
        //                content.title = "title"
        //                content.subtitle = "subTitle"
        content.body = "こんにちわ"
        content.badge = NSNumber(value: 1)
        content.sound = UNNotificationSound.default()
        var trigger: UNNotificationTrigger?
        // 時間と曜日設定
        //(日曜:1~)
        trigger = UNCalendarNotificationTrigger(dateMatching: DateComponents(hour:16, minute:47, weekday:4), repeats: false)
        let request = UNNotificationRequest(identifier: notificationIdentifier, content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request)
        
        
        // アラート
        let alert = UIAlertController(title:nil, message: "登録しました。", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {
            (action: UIAlertAction!) in
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

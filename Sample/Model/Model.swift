//
//  Model.swift
//  Sample
//
//  Created by abc on 2018/03/20.
//  Copyright © 2018年 abc All rights reserved.
//

import UIKit

class Model: NSObject {
    
    var name: String?
    
    convenience required init(json: [String: Any]) {
        self.init()
        
        if let name = json["name"] as? String {
            self.name = name
        }
    }
}

//
//  AppDelegate.swift
//  Sample
//
//  Created by abc on 2018/03/20.
//  Copyright © 2018年 abc All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // 通知設定アラート
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        // バッジを非表示にしている .badge,
        center.requestAuthorization(options: [.alert, .sound]) {granted, error in
            if error != nil {
                // エラー時の処理
                print("エラーが発生しました：[\(String(describing: error))]")
                return
            }
            if granted {
                // デバイストークンの要求
                OperationQueue.main.addOperation({
                    UIApplication.shared.registerForRemoteNotifications()
                })
            }
        }
        
        return true
    }
    
    /**
     通知を受け取った際に入るメソッド
     */
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // 通知の情報を取得
        let notification = response.notification
        
        // リモート通知かローカル通知かを判別
        if notification.request.trigger is UNPushNotificationTrigger {
            print("didReceive Push Notification")
        } else {
            print("didReceive Local Notification")
        }
        
        // 通知の ID を取得
        print("notification.request.identifier: \(notification.request.identifier)")
        
        // 処理完了時に呼ぶ
        completionHandler()
    }
    
    /**
     アプリ起動中にプッシュ通知が表示されるように設定
     */
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // アプリ起動中でもアラート&音で通知
        completionHandler([.alert, .sound])
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

